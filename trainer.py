from tqdm import tqdm
import torch
from torch.autograd import Variable as Var
from utils import map_label_to_target, map_label_to_target_sentiment
import torch.nn.functional as F

class SentimentTrainer(object):
    """
    For Sentiment module
    """
    def __init__(self, args, model, embedding_model ,criterion, optimizer):
        super(SentimentTrainer, self).__init__()
        self.args       = args
        self.model      = model
        self.embedding_model = embedding_model
        self.criterion  = criterion
        self.optimizer  = optimizer
        self.epoch      = 0

    # helper function for training
    def train(self, dataloader, dataset):
        self.model.train()
        self.embedding_model.train()
        self.embedding_model.zero_grad()
        self.optimizer.zero_grad()
        loss, k = 0.0, 0
        # indices = torch.randperm(len(dataset))
        # for idx in tqdm(xrange(len(dataset)),desc='Training epoch '+str(self.epoch+1)+''):
        totalk = 0
        two = 0
        for iteration, batch in enumerate(tqdm(dataloader,desc='Training epoch '+str(self.epoch+1)+''), 1):
            sent, label = batch
            input = Var(sent)
            target = Var(label.long())
            if self.args.cuda:
                input = input.cuda()
                target = target.cuda()
                totalk += len(target)
            two += sum(target==2)
            emb = self.embedding_model(input)
            output = self.model.forward(emb, training = True)
            # params = self.model.childsumtreelstm.getParameters()
            # params_norm = params.norm().data[0] # we do not need variable here, params_norm is float, prevent GPU-memory leak
            params = None # prevent GPU-memory leak
            err = self.criterion(output, target)
            loss += err.data[0] #
            err.backward()
            k += 1

            for f in self.embedding_model.parameters():
                f.data.sub_(f.grad.data * self.args.emblr)
            self.optimizer.step()
            self.embedding_model.zero_grad()
            self.optimizer.zero_grad()
        self.epoch += 1
        return loss/len(dataset)

    # helper function for testing
    def test(self, dataloader, dataset, allow_neutral = False):
        self.model.eval()
        self.embedding_model.eval()
        loss = 0
        # predictions = torch.zeros(len(dataset))
        # predictions = predictions
        # indices = torch.range(1,dataset.num_classes)
        predictions = []
        for iteration, batch in enumerate(tqdm(dataloader, desc='Testing epoch ' + str(self.epoch) + ''), 1):
            sent, label = batch
            input = Var(sent, volatile=True)
            target = Var(label.long())
            if self.args.cuda:
                input = input.cuda()
                target = target.cuda()
            emb = self.embedding_model(input)
            output = self.model(emb) # size(1,5)
            err = self.criterion(output, target)
            loss += err.data[0]
            if not allow_neutral:
                output[:,1] = -9999 # no need middle (neutral) value
            val, pred = torch.max(output, 1)
            predictions.append(pred)

            # predictions[idx] = pred.data.cpu()[0][0]
            # predictions[idx] = torch.dot(indices,torch.exp(output.data.cpu()))
        predictions = F.torch.cat(predictions)
        predictions = predictions.data.cpu()
        predictions = torch.squeeze(predictions, 1)
        return loss/len(dataset), predictions
