import torch
import torch.nn as nn
from torch.autograd import Variable as Var
import torch.nn.functional as F

class SimpleLSTM(nn.Module):
    def __init__(self, n_units, cuda):
        super(SimpleLSTM, self).__init__()

        self.lstm = nn.LSTM(input_size=n_units, hidden_size=n_units, batch_first=True)
        self.l = nn.Linear(n_units, 3)

    def forward(self, emb, training = False):
        o, hn = self.lstm(emb)
        h, c = hn
        h = torch.squeeze(h, 0)
        output = self.l(h)
        return output