import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import math
from math import log
from slstm import SLSTM


class NTIsentiment(nn.Module):
    def __init__(self, n_units, cuda):
        super(NTIsentiment, self).__init__()
        self.cudaFlag = cuda
        self.h_lstm = nn.LSTMCell(n_units, n_units)  # use
        self.h_x = nn.Linear(n_units, n_units)  #
        self.h_h = nn.Linear(n_units, n_units)  #

        self.slstm = SLSTM(cuda, n_units, n_units)

        # 2 layer MLP
        self.l1 = nn.Linear(300, 200)
        self.l2 = nn.Linear(200, 200)
        self.l3 = nn.Linear(200, 3)  # 3 class (good, neutral ,bad)

        self.__n_units = n_units
        self.cudaFlag = cuda

        if self.cudaFlag:
            self.l1 = self.l1.cuda()
            self.l2 = self.l2.cuda()
            self.l3 = self.l3.cuda()
            self.h_lstm = self.h_lstm.cuda()
            self.h_x = self.h_x.cuda()
            self.h_h = self.h_h.cuda()

    def forward(self, x_batch, training = False):
        """

        :param x_batch: (batch_size, seq len, features)
        :param training:
        :return:
        """
        batch_size = len(x_batch)
        x_len = x_batch.size(1)
        depth = int(log(x_len, 2)) + 1

        list_a = [[] for i in range(2**depth-1)]
        list_c = [[] for i in range(2**depth-1)]

        h = Variable(torch.zeros(batch_size, self.__n_units))
        c = Variable(torch.zeros(batch_size, self.__n_units))
        if self.cudaFlag:
            h = h.cuda()
            c = c.cuda()

        for l in xrange(x_len):
            # get every token
            # x_data = torch.Tensor([x_batch[k][l] for k in range(batch_size)])
            # x_data = Variable(x_data)
            x_data = x_batch[:,l]

            h, c = self.h_lstm(F.dropout(x_data, p=0.2, training = training), (h, c))
            list_a[x_len - 1 + l] = h
            list_c[x_len - 1 + l] = c  # Variable(zeros, volatile=not train)

        for d in reversed(range(1, depth)):
            for s in range(2**d-1, 2**(d+1)-1, 2):
                l = self.h_x(F.dropout(list_a[s], p=0.2, training=training))
                r = self.h_h(F.dropout(list_a[s+1], p=0.2, training=training))
                c_l = list_c[s]
                c_r = list_c[s+1]
                c, h = self.slstm.forward(c_l, c_r, l, r)
                list_a[(s-1)/2] = h
                list_c[(s-1)/2] = c

        h_root = list_a[0]

        h1 = F.tanh(self.l1(F.dropout(h_root, p=0.2, training=training)))
        h2 = F.tanh(self.l2(F.dropout(h1, p=0.2, training=training)))
        y = F.log_softmax(self.l3(F.dropout(h2, p=0.2, training=training)))
        # y = F.softmax(self.l3(F.dropout(h2, p=0.2, training=training)))

        return y