import torch
import torch.nn as nn
import torch.functional as F

class NTIFullTreeMatching(nn.Module):
    def __init__(self, n_units, gpu):
        h_lstm = nn.LSTMCell(n_units, n_units)
        m_lstm = nn.LSTMCell(n_units, n_units)
        h_x = nn.Linear(n_units, 4*n_units)
        w_ap = nn.Linear(n_units, n_units), # attend
        w_we = nn.Linear(n_units, 1), # no where to be found
        w_c = nn.Linear(n_units, n_units), # attend
        w_q = nn.Linear(n_units, n_units), # attend
        h_l1 = nn.Linear(2*n_units, 1024), # 2*n_units because concat 2 word (need modify for sentiment)
        l_y = nn.Linear(1024, 3)  # get final output

        self.__n_units = n_units
        self.__gpu = gpu
        
