import torch
import torch.nn as nn
import torch.nn.functional as F

class SLSTM(nn.Module):
    def __init__(self, cuda, in_dim, mem_dim):
        super(SLSTM, self).__init__()
        self.cudaFlag = cuda
        self.in_dim = in_dim
        self.mem_dim = mem_dim

        # model
        self.WLhi = nn.Linear(mem_dim, mem_dim)
        self.WRhi = nn.Linear(mem_dim, mem_dim)
        self.WLci = nn.Linear(mem_dim, mem_dim)
        self.WRci = nn.Linear(mem_dim, mem_dim)

        self.WLhfl = nn.Linear(mem_dim, mem_dim)
        self.WRhfl = nn.Linear(mem_dim, mem_dim)
        self.WLcfl = nn.Linear(mem_dim, mem_dim)
        self.WRcfl = nn.Linear(mem_dim, mem_dim)

        self.WLhfr = nn.Linear(mem_dim, mem_dim)
        self.WRhfr = nn.Linear(mem_dim, mem_dim)
        self.WLcfr = nn.Linear(mem_dim, mem_dim)
        self.WRcfr = nn.Linear(mem_dim, mem_dim)

        self.WLhx = nn.Linear(mem_dim, mem_dim)
        self.WRhx = nn.Linear(mem_dim, mem_dim)

        self.WLho = nn.Linear(mem_dim, mem_dim)
        self.WRho = nn.Linear(mem_dim, mem_dim)
        self.Wco = nn.Linear(mem_dim, mem_dim)

    def forward(self, c_left, c_right, h_left, h_right):
        i = F.sigmoid(self.WLhi(h_left) + self.WRhi(h_right) + \
                      self.WLci(c_left) +self.WRci(c_right))


        fl = F.sigmoid(self.WLhfl(h_left) + self.WRhfl(h_right) + \
                      self.WLcfl(c_left) +self.WRcfl(c_right))

        fr = F.sigmoid(self.WLhfr(h_left) + self.WRhfr(h_right) + \
                       self.WLcfr(c_left) + self.WRcfr(c_right))

        x = self.WLhx(h_left) + self.WRhx(h_right)

        c = fl * c_left + fr * c_right + i*F.tanh(x)

        o = F.sigmoid(self.WLho(h_left) + self.WRho(h_right) + self.Wco(c))

        h = o*F.tanh(c)

        return c, h