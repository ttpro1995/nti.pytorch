# glove dir DXopjCBnJhPQoxF52zFMDn
# sst dir 
# floyd run --gpu --env pytorch:py2 --data DXopjCBnJhPQoxF52zFMDn:glove --data cYgEayanNYjCELPoywWUnX:sst "sh run.sh"
pip install -U meowlogtool
cp -r /sst .
cp -r /glove .

python sentiment.py --name gen_ds --data sst/ --glove glove/ --logs /output  
echo 'done preprocess ds'
python sentiment.py --name r4 --data sst/ --glove glove/ --logs /output  --epochs 30 --lr 1e-3 --wd 1e-5 --optim adam &
python sentiment.py --name r5 --data sst/ --glove glove/ --logs /output  --epochs 30 --lr 1e-3 --wd 2e-5 --optim adam &
python sentiment.py --name r6  --data sst/ --glove glove/ --logs /output  --epochs 30 --lr 1e-3 --wd 3e-5 --optim adam &
python sentiment.py --name r7  --data sst/ --glove glove/ --logs /output  --epochs 30 --lr 1e-3 --wd 1e-4 --optim adam &
python sentiment.py --name r8  --data sst/ --glove glove/ --logs /output  --epochs 30 --lr 1e-4 --wd 1e-4 --optim adam &

wait
echo 'done'
